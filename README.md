# buildbox-node

NodeJS application development image based on [buildpack-node](https://git.it-consultis.net/docker/buildpack-node)

## Image features

- docker-ce
- ansible:2.0
- precompiled node-sass binary at SASS_BINARY_PATH

## Volumes

`/var/run/docker.sock`

You need to mount this from this host if you want to build Docker images from
inside the container. 
