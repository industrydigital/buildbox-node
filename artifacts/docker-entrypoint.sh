#!/usr/bin/env bash
set -aeu

if [ "$(whoami)" == "root" ]; then
    if [ -v "DOCKER_HOST_UID" ]; then
        usermod -u $DOCKER_HOST_UID node
    fi
    if [ -v "DOCKER_HOST_GID" ]; then
        # tolerate failure here (happens on Mac OSX)
        groupmod -g $DOCKER_HOST_GID node || true
    fi
    chown -R node:node /home/node
fi

exec "$@"

