FROM industrydigital/buildpack-node:12.10.0
MAINTAINER Rich Choy <rich.choy@industrydigital.io>

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL en_US.UTF-8 
ENV LANG en_US.UTF-8

RUN set -x \
    && DEBIAN_FRONTEND=noninteractive \
    && LC_ALL=C.UTF-8 \
    && LANG=en_US.UTF-8 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 93C4A3FD7BB9C367 \
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
    && apt-get update \
    && apt-get install -y --reinstall --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        software-properties-common \
    && apt-add-repository ppa:ansible/ansible \
    && apt-add-repository ppa:git-core/ppa \
    && apt-add-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
    && apt-get update \
    && curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash \
    && apt-get install -y --no-install-recommends \
        ansible=$(apt-cache madison ansible | grep ppa | cut -d'|' -f 2 | xargs) \
        curl \
        docker-ce \
        freetype* \
        git \
        git-lfs \
        openssh-client \
        rsync \
    && update-ca-certificates \
    && apt-get purge -y lxc-docker || true \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && which git-lfs \
    && which docker

COPY ./artifacts/etc/ssh/ssh_config /etc/ssh/ssh_config
